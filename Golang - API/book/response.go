package book

type BookResponse struct {
	ID        int `json:"id"`
	Title     string `json:"title"`
	Desc      string `json:"desc"`
	Price     int `json:"price"`
	Rating    int `json:"rating"`
}