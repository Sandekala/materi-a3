package book

import (
	"fmt"
)

type fileRepository struct{}

func NewFileRepository() *fileRepository {
	return &fileRepository{}
}

func (r *fileRepository) FindAll() ([]Book, error) {
	var books []Book
	fmt.Println("findAll")
	return books, nil
}

func (r *fileRepository) FindByID(ID int) (Book, error) {
	var book Book
	fmt.Println("findByID")

	return book, nil
}

func (r *fileRepository) Create(book Book) (Book, error) {
	fmt.Println("create")

	return book, nil
}
