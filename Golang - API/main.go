package main

import (
	"golang-api/book"
	"golang-api/handler"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	// connect db	
	dsn := "root@tcp(127.0.0.1:3306)/golang_db?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("DB connection error!")
	}
	db.AutoMigrate(&book.Book{})

	bookRepository := book.NewRepository(db)
	// bookFileRepository := book.NewFileRepository()
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)




	// CRUD menggunakan layer repository



	// bookRequest := book.BookRequest{
	// 	Title: "Nyi Roro Kidul",
	// 	Price: "50000",
	// }
	// bookService.Create(bookRequest)

	// books, err := bookRepository.FindAll()

	// for _, book := range books {
	// 	fmt.Println("Title : ", book.Title)
	// }

	// CRUD

	// Create

	// book := book.Book{}
	// book.Title = "Nyiroro Kidul"
	// book.Price = 30
	// book.Rating = 6
	// book.Desc = "Legenda laut kidul"

	// err = db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println("DB connection error!")
	// }

	// Read

	// var book book.Book
	//  err = db.First(&book).Error
	//  if err != nil {
	//  	fmt.Println("DB connection error!")
	//  }
	//  fmt.Printf("Judul buku: %v, Desc: %v", book.Title, book.Desc)

	// Update

	// var book book.Book
	//  err = db.Where("id = ?", 1).First(&book).Error
	//  if err != nil {
	//  	fmt.Println("DB connection error!")
	//  }
	//  book.Title = "Kuyang"
	//  book.Desc = "Legenda Kalimantan"
	//  err = db.Save(&book).Error

	//  if err != nil {
	// 	fmt.Println("DB connection error!")
	// }

	// Delete

	// var book book.Book
	//  err = db.Where("id = ?", 1).First(&book).Error
	//  if err != nil {
	//  	fmt.Println("DB connection error!")
	//  }
	//  err = db.Delete(&book).Error
	//  if err != nil {
	//  	fmt.Println("DB connection error!")
	//  }



	router := gin.Default()
	v1 := router.Group("/v1")

	v1.GET("/books", bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetBook)
	v1.POST("/books", bookHandler.PostBooksHandler)
	v1.PATCH("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)
	router.Run()
}