package main

import (
	"context"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"golang-mongodb/controllers"
)

func main() {
	r := httprouter.New()
	uc := controllers.NewUserController(getSession())
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.RemoveUser)
	http.ListenAndServe("localhost:8080", r)
}

// func getSession() *mgo.Session {
// 	uri := os.Getenv("mongodb+srv://sandekala:123@sandekalacluster.3xa18h4.mongodb.net/?retryWrites=true&w=majority")
// 	s, err := mgo.Dial(uri)
// 	if err != nil {
// 		panic(err)
// 	}
// 	return s
// }

func getSession() *mongo.Client {
    clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
    s, err := mongo.NewClient(clientOptions)
    if err != nil {
        log.Fatal(err)
    }
    err = s.Connect(context.Background())
    if err != nil {
        log.Fatal(err)
    }
    return s
}